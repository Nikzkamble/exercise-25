﻿using System;

namespace exercise_25
{
    /*class person
    {
     private string name;
         private int age;
        public person(string _name,int _age)
        {
            name=_name;
            age=_age;
          }
          void fullname()
          {
              Console.WriteLine($"My name is {name},my age is {age}");
          }*/
    public class Checkout
    {
        public int quantity;
        public double price;

        public Checkout(int _quantity, double _price)
        {
            quantity = _quantity;
            price = _price;
            
        }

        public double total()
        {
            return (quantity * price);
        }

        static void Main(string[] args)
        {
            Console.Clear();

            Checkout a1 = new Checkout(1, 15);
            Checkout a2 = new Checkout(1, 15);
            Checkout a3 = new Checkout(1, 15);

            /*var abc= new person("nik",19);  
            abc.fullname();  */

            Console.WriteLine(a1.total() + a2.total()+ a3.total());

            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
